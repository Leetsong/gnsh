# NSH

> This is a naive shell, named nsh

<p style="text-align:center"><img src="./.bitbucket/nsh.png" alt="nsh" style="width: 600px; box-shadow: 0 10px 20px rgba(0,0,0,0.19), 0 6px 6px rgba(0,0,0,0.23);"></p>

### get it

``` sh
$ git clone https://Leetsong@bitbucket.org/Leetsong/nsh.git
```

### run it

``` sh
# dev run
$ make clean && make dev && make run

# production build
$ make clean && make build
```
package logger

import ( "fmt"; "runtime" )

type Logger interface {
	Log(format string, args ...interface{})
	Logln(format string, args ...interface{})
	Logc(format string, args ...interface{})  // common
	Logi(format string, args ...interface{})  // info
	Logd(format string, args ...interface{})  // debug
	Logw(format string, args ...interface{})  // warning
	Loge(format string, args ...interface{})  // error
}

type Colorffer interface {
	Normal(format string, args ...interface{}) string
	Bold(format string, args ...interface{}) string
	Underline(format string, args ...interface{}) string
	Red(format string, args ...interface{}) string
	Green(format string, args ...interface{}) string
	Yellow(format string, args ...interface{}) string
}

const (
	LOGGER_LEVEL_E = iota + 1
	LOGGER_LEVEL_W
	LOGGER_LEVEL_D
	LOGGER_LEVEL_I
	LOGGER_LEVEL_N
)

type logger struct {
	level int
}

func NewLogger(level int) (Logger, Colorffer) {
	l := &logger{ level }
	return l, l
}

func NewColorffer() Colorffer {
	l := &logger{ LOGGER_LEVEL_N }
	return l
}

func (self *logger) SetLevel(level int) {
	self.level = level
}

func (self *logger) Level() int {
	return self.level
}

func (self *logger) Log(format string, args ...interface{}) {
	fmt.Print(self.Bold(format, args...))
}

func (self *logger) Logln(format string, args ...interface{}) {
	fmt.Println(self.Bold(format, args...))
}

func (self *logger) Logc(format string, args ...interface{}) {
	if _, file, line, ok := runtime.Caller(0); ok {
		fmt.Printf(self.Bold(self.Underline("common") + " %s:%d\n", file, line))
		self.Logln(format, args...)
	}
}

func (self *logger) Logi(format string, args ...interface{}) {
	if self.level > LOGGER_LEVEL_I {
		if _, file, line, ok := runtime.Caller(0); ok {
			fmt.Printf(self.Bold(self.Underline(self.Green("info")) + " %s:%d\n", file, line))
		}
		self.Logln(format, args...)
	}
}

func (self *logger) Logd(format string, args ...interface{}) {
	if self.level > LOGGER_LEVEL_D {
		if _, file, line, ok := runtime.Caller(0); ok {
			fmt.Printf(self.Bold(self.Underline(self.Green("debug")) + " %s:%d\n", file, line))
		}
		self.Logln(format, args...)
	}
}

func (self *logger) Logw(format string, args ...interface{}) {
	if self.level > LOGGER_LEVEL_W {
		if _, file, line, ok := runtime.Caller(0); ok {
			fmt.Printf(self.Bold(self.Underline(self.Yellow("warning")) + " %s:%d\n", file, line))
		}
		self.Logln(format, args...)
	}
}

func (self *logger) Loge(format string, args ...interface{}) {
	if self.level > LOGGER_LEVEL_E {
		if _, file, line, ok := runtime.Caller(0); ok {
			fmt.Printf(self.Bold(self.Underline(self.Red("error")) + " %s:%d\n", file, line))
		}
		self.Logln(format, args...)
	}
}

func (self *logger) Normal(format string, args ...interface{}) string {
	return fmt.Sprintf("\x1b[0m" + format, args...)
}

func (self *logger) Bold(format string, args ...interface{}) string {
	return fmt.Sprintf("\x1b[1m" + format + "\x1b[0m", args...)
}

func (self *logger) Underline(format string, args ...interface{}) string {
	return fmt.Sprintf("\x1b[4m" + format + "\x1b[0m", args...)
}

func (self *logger) Red(format string, args ...interface{}) string {
	return fmt.Sprintf("\x1b[1;31m" + format + "\x1b[0m", args...)
}

func (self *logger) Green(format string, args ...interface{}) string {
	return fmt.Sprintf("\x1b[1;32m" + format + "\x1b[0m", args...)
}

func (self *logger) Yellow(format string, args ...interface{}) string {
	return fmt.Sprintf("\x1b[1;33m" + format + "\x1b[0m", args...)
}
